# Artikel
---
Aufgabe zur Programmierung mit Python. Programm dient zum Anlegen, Einlesen, Löschen und Editieren von Artikeln. 


# Lizenz
---
Verteilt unter der Apache Lizenz Version 2.0.

# Autor
---
* ComradeBlack (https://bitbucket.org/ComradeBlack/)

